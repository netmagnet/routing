<?php

namespace Slts\Routing\Routers;

use RuntimeException;

class FilteredParamRejectedException extends RuntimeException
{
}
