<?php

namespace Slts\Routing\Routers;

use Nette\Application\Routers\Route;
use Nette\Http\IRequest;
use Nette\Http\UrlScript;
use Nette\Routing\Router;

/**
 * Slightly modified solution https://pla.nette.org/cs/routovani-vice-parametru-ve-filtru
 * cause man, translatable URL invented by Koubic are totally fucked up thing
 */
class FilteredRoute extends Route
{
    public const WAY_IN = 'in';
    public const WAY_OUT = 'out';

    protected $filters = [];

    protected $enabledFixQueryStringArrayNumeration = false;

    protected $fallbackUrlParams = [];
    protected $fallbackUrlParamsFromPreviousRequest = [];
    /** @var Router $router */
    protected $router;

    public function __construct(string $mask, $metadata = [], int $flags = 0)
    {
        $keysToSlice = array_flip([self::WAY_IN, self::WAY_OUT]);

        $slicedMetadata = [];
        foreach ($metadata as $param => $paramMetadata) {
            if (is_array($paramMetadata)) {
                $filters = array_intersect_key($paramMetadata, $keysToSlice);
                foreach ($filters as $way => $filter) {
                    $this->addFilter($param, $way, $filter);
                }
                $slicedMetadata[$param] = array_diff_key($paramMetadata, $keysToSlice);
                continue;
            }

            $slicedMetadata[$param] = $paramMetadata;
        }

        parent::__construct($mask, $slicedMetadata, $flags);
    }

    public function match(IRequest $httpRequest): ?array
    {
        $appRequest = parent::match($httpRequest);
        if (null === $appRequest) {
            return null;
        }

        try {
            $filteredParams = $this->doFilterParams($appRequest, self::WAY_IN);
        } catch (FilteredParamRejectedException $e) {
            return null;
        }

        return $filteredParams;
    }

    public function constructUrl(array $appRequest, UrlScript $refUrl): ?string
    {
        try {
            $filteredParams = $this->doFilterParams($appRequest, self::WAY_OUT);
        } catch (FilteredParamRejectedException $e) {
            return $this->tryCreateFallback($appRequest, $refUrl);
        }

        $str = parent::constructUrl($filteredParams, $refUrl);
        if (null === $str) {
            return $this->tryCreateFallback($appRequest, $refUrl);
        }

        return $this->fixQueryStringArrayNumeration($str);
    }

    /**
     * @see http://php.net/manual/en/function.http-build-query.php#111819
     *
     * @param string|null $str
     *
     * @return string|null
     */
    protected function fixQueryStringArrayNumeration(?string $str): ?string
    {
        if (null === $str) {
            return null;
        }
        if ($this->enabledFixQueryStringArrayNumeration) {
            return preg_replace('/%5B\d+%5D/imU', '%5B%5D', $str);
        }
        return $str;
    }

    /**
     * @param string   $param
     * @param string   $way
     * @param callable $filter
     */
    public function addFilter(string $param, string $way, $filter): void
    {
        $this->filters[$way][$param] = $filter;
    }

    /**
     * @param bool $enabledFixQueryStringArrayNumeration
     */
    public function setEnabledFixQueryStringArrayNumeration(bool $enabledFixQueryStringArrayNumeration): void
    {
        $this->enabledFixQueryStringArrayNumeration = $enabledFixQueryStringArrayNumeration;
    }

    protected function doFilterParams(array $params, string $way): array
    {
        if (!isset($this->filters[$way])) {
            return $params;
        }

        $filtered = [];
        foreach ($this->filters[$way] as $paramName => $filter) {
            $valueToFilter = $params[$paramName] ?? null;
            $filtered[$paramName] = $filter($valueToFilter, $params);
        }

        return array_merge($params, $filtered);
    }

    protected function tryCreateFallback(array $requestParams, UrlScript $refUrl)
    {
        if ($this->router instanceof Router && $this->fallbackUrlParams) {
            $finalFallbackParams = $this->prepareFinalFallbackUrlParams($requestParams);
            $str = $this->router->constructUrl($finalFallbackParams, $refUrl);
            return $this->fixQueryStringArrayNumeration($str);
        }
        return null;
    }

    protected function prepareFinalFallbackUrlParams(array $requestParams): array
    {
        $paramsFromRequest = array_intersect_key($requestParams, array_flip($this->fallbackUrlParamsFromPreviousRequest));

        return array_merge($this->fallbackUrlParams, $paramsFromRequest);
    }

    public function setFallbackUrlParams(Router $router, array $params, array $paramsFromPreviousRequest): void
    {
        $this->router = $router;
        $this->fallbackUrlParams = $params;
        $this->fallbackUrlParamsFromPreviousRequest = $paramsFromPreviousRequest;
    }
}
